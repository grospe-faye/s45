import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(){

	// Allows us to consume the User Context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	console.log(email)
	console.log(password)

	function LoginUser(e) {
		
		e.preventDefault()

		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful!',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})
			} else {
				Swal.fire({
					title: 'Authentication Failed.',
					icon: 'error',
					text: 'Check you login details'
				})
			}
		})

		// Set the email of the user in the local storage
		// Syntax:
			// localStorage.setItem('propertyName, value')
		// localStorage.setItem('email', email)

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail('')
		setPassword('')

		// alert('You are now logged in.')
	}

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization : `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else{
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id !== null) ? 
			<Redirect to='/courses'/>
			:		
			<Form onSubmit={(e) => LoginUser(e)}>

				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
						type='email'
	 					placeholder='Enter email'
	 					value={email}
	 					onChange={e => setEmail(e.target.value)}
	 					required
					/>
				</Form.Group>

				<Form.Group controlId='password'>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type='password'
						placeholder='Enter password'
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>
				{ isActive ? 
					<Button variant='success' type='submit' id='submitBtn'>
					Login
					</Button>

					: 
					<Button variant='danger' type='submit' id='submitBtn' disabled>
					Login	
					</Button>
				}
			</Form>
	)
}