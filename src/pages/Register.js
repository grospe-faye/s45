import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect, useHistory} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register(){

	// State hooks to store the values of the input fields
	const {user, setUser} = useContext(UserContext)
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	// State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	const history = useHistory()

	console.log(firstName)
	console.log(lastName)
	console.log(email)
	console.log(mobileNo)
	console.log(password1)
	console.log(password2)

	function registerUser(e){

		// prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Duplicate email found.',
					icon: 'error',
					text: 'Please provide a diffrerent email'
				})
			} else {
				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
 						firstName : firstName,
 						lastName : lastName,
 						email : email,
 						mobileNo : mobileNo,
 						password: password1
 					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data === true){
						Swal.fire({
							title: 'Registration successful',
							icon: 'success',
							text: 'Welcome to Zuitt'
						})
						history.push('/login')
					}
				})
			}
		})

		// localStorage.setItem('email', email)

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// clears the input fields
		setEmail('')
		setPassword1('')
		setPassword2('')
		setFirstName('')
		setLastName('')

		// alert('Thank you for registering!')
	}


	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNo !=='' ) && (password1 === password2)){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password1, password2, firstName, lastName, mobileNo])

	return(
		(user.id !== null) ?
			<Redirect to='/courses' />
			:
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control
 					type='text'
 					placeholder='Enter first name'
 					value={firstName}
 					onChange={e => setFirstName(e.target.value)}
 					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
 					type='text'
 					placeholder='Enter last name'
 					value={lastName}
 					onChange={e => setLastName(e.target.value)}
 					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
 					type='email'
 					placeholder='Please enter your email here'
 					value={email}
 					onChange={e => setEmail(e.target.value)}
 					required
				/>
				<Form.Text className='text-muted'>
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
 					type='text'
 					maxLength="11" pattern="\d{11}"
 					placeholder='Enter Mobile Number'
 					value={mobileNo}
 					onChange={e => setMobileNo(e.target.value)}
 					required
				/>
			</Form.Group>

			<Form.Group controlId='password1'>
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type='password'
					placeholder='Please input your password here'
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId='password2'>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
 					type='password'
 					placeholder='Please Verify your password'
 					value={password2}
					onChange={e => setPassword2(e.target.value)}
 					required
				/>
			</Form.Group>
			{ isActive ? 
				<Button variant='primary' type='submit' id='submitBtn'>
				Register
				</Button>

				: 
				<Button variant='danger' type='submit' id='submitBtn' disabled>
				Register	
				</Button>
			}
		</Form>

	)
}